

document.addEventListener('keydown', function(event) {
    // Check if Ctrl (or Command on Mac) and Y are pressed together
    if ((event.ctrlKey || event.metaKey) && event.key === 'j') {
        event.preventDefault();  // Prevent any default behavior (optional)
        
        // Your script to run goes here
        function setFishingCookie() {
    // Ask the user for input
    const input = prompt("Please enter the amount of money earned:");
    
    // Set the cookie with the user input
    document.cookie = `GoneFishingmoneyEarned=${input};`;
    
    // Reload the page
    location.reload();
}

// Call the function to execute the script
setFishingCookie();
        
        // For example, you can call another function here
        // runMyFunction();
    }
});

